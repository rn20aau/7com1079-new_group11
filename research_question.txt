Group: 7COM1079-new_group11  

Question

RQ: Is there a difference in driving distances of a golf ball by coating it?

Null hypothesis: New coating does not have effect on driving distances 

Alternate hypothesis: New coating does have significant effect on driving distances


Dataset

URL: https://www.kaggle.com/ipravin/golf-ball-testing-data-set-from-par-inc/download

Column Headings:

"Current" "New"
{
  "nbformat": 4,
  "nbformat_minor": 0,
  "metadata": {
    "colab": {
      "name": "ht_R.ipynb",
      "provenance": [],
      "collapsed_sections": []
    },
    "kernelspec": {
      "name": "python3",
      "display_name": "Python 3"
    },
    "accelerator": "GPU"
  },
  "cells": [
    {
      "cell_type": "code",
      "metadata": {
        "id": "Vyo9TRgPvBqi",
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "outputId": "12c3bd6e-acce-432c-a32a-63cda9d6f8bd"
      },
      "source": [
        "# activate R magic\r\n",
        "%load_ext rpy2.ipython"
      ],
      "execution_count": 8,
      "outputs": [
        {
          "output_type": "stream",
          "text": [
            "The rpy2.ipython extension is already loaded. To reload it, use:\n",
            "  %reload_ext rpy2.ipython\n"
          ],
          "name": "stdout"
        }
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "KFwWRMJJwG3s"
      },
      "source": [
        "%%R\r\n",
        "library(\"readxl\")\r\n",
        "library(dplyr)\r\n",
        "library(ggplot2)"
      ],
      "execution_count": 9,
      "outputs": []
    },
    {
      "cell_type": "code",
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "a1ynBYUhr907",
        "outputId": "9e1cec39-03e0-41a3-f9dc-c6c151b5dd1b"
      },
      "source": [
        "%R\r\n",
        "from google.colab import drive\r\n",
        "drive.mount('/content/drive')"
      ],
      "execution_count": 3,
      "outputs": [
        {
          "output_type": "stream",
          "text": [
            "Mounted at /content/drive\n"
          ],
          "name": "stdout"
        }
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "L2vqxOzF2CE7"
      },
      "source": [
        "###Hypothesis Testing Using Student's T-test###"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "c8xFkr9s2Z-E"
      },
      "source": [
        "#About the data from Kaggle #\r\n",
        "Par Inc., is a major manufacturer of golf equipment. Management believes that Par’s market share could be increased with the introduction of a cut-resistant, longer-lasting golf ball. Therefore, the research group at Par has been investigating a new golf ball coating designed to resist cuts and provide a more durable ball. The tests with the coating have been promising. One of the researchers voiced concern about the effect of the new coating on driving distances. Par would like the new cut-resistant ball to offer driving distances comparable to those of the current-model golf ball. To compare the driving distances for the two balls, 40 balls of both the new and current models were subjected to distance tests. The testing was performed with a mechanical hitting machine so that any difference between the mean distances for the two models could be attributed to a difference in the design."
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "xdR7YbX4zSx6"
      },
      "source": [
        "Reading Data from Google Drive"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "Ygdm-NiGuH7s",
        "outputId": "0f5cb26e-b015-46fe-e68d-f20de7fab634"
      },
      "source": [
        "%%R\r\n",
        "path='/content/drive/My Drive/Golf.csv'\r\n",
        "golf <- read.csv(path , header = T)\r\n",
        "golf"
      ],
      "execution_count": 10,
      "outputs": [
        {
          "output_type": "stream",
          "text": [
            "   Current New\n",
            "1      264 277\n",
            "2      261 269\n",
            "3      267 263\n",
            "4      272 266\n",
            "5      258 262\n",
            "6      283 251\n",
            "7      258 262\n",
            "8      266 289\n",
            "9      259 286\n",
            "10     270 264\n",
            "11     263 274\n",
            "12     264 266\n",
            "13     284 262\n",
            "14     263 271\n",
            "15     260 260\n",
            "16     283 281\n",
            "17     255 250\n",
            "18     272 263\n",
            "19     266 278\n",
            "20     268 264\n",
            "21     270 272\n",
            "22     287 259\n",
            "23     289 264\n",
            "24     280 280\n",
            "25     272 274\n",
            "26     275 281\n",
            "27     265 276\n",
            "28     260 269\n",
            "29     278 268\n",
            "30     275 262\n",
            "31     281 283\n",
            "32     274 250\n",
            "33     273 253\n",
            "34     263 260\n",
            "35     275 270\n",
            "36     267 263\n",
            "37     279 261\n",
            "38     274 255\n",
            "39     276 263\n",
            "40     262 279\n"
          ],
          "name": "stdout"
        }
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "QIG81byWzZha"
      },
      "source": [
        "Dimension Identification"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "eLehDCVZuwH6",
        "outputId": "0501cc79-8d33-445e-9b15-4f95122d0af5"
      },
      "source": [
        "%%R\r\n",
        "dim(golf)"
      ],
      "execution_count": 11,
      "outputs": [
        {
          "output_type": "stream",
          "text": [
            "[1] 40  2\n"
          ],
          "name": "stdout"
        }
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "w7H80_OPzXt4"
      },
      "source": [
        "Variable Identification"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "tt26nmWXu6rS",
        "outputId": "c0328d0b-4dfa-47d0-d0a7-4eb0335230cd"
      },
      "source": [
        "%%R\r\n",
        "names(golf)"
      ],
      "execution_count": 12,
      "outputs": [
        {
          "output_type": "stream",
          "text": [
            "[1] \"Current\" \"New\"    \n"
          ],
          "name": "stdout"
        }
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "L_oB5_M6vSP7",
        "outputId": "a6e5d292-3b39-4f50-c4b2-12087f9cd7b1"
      },
      "source": [
        "%%R\r\n",
        "str(golf)"
      ],
      "execution_count": 13,
      "outputs": [
        {
          "output_type": "stream",
          "text": [
            "'data.frame':\t40 obs. of  2 variables:\n",
            " $ Current: int  264 261 267 272 258 283 258 266 259 270 ...\n",
            " $ New    : int  277 269 263 266 262 251 262 289 286 264 ...\n"
          ],
          "name": "stdout"
        }
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "Yolkm3a-zq_S"
      },
      "source": [
        "Univariate Analysis:"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "Obi87QmHvSW9",
        "outputId": "253fa8bf-ead2-4264-c23a-5a4c04a41fa6"
      },
      "source": [
        "%%R\r\n",
        "attach(golf)\r\n",
        "summary(golf)\r\n",
        "cat(\"SD for Variable Current: \" , sd(Current) , \"; SD for variable New: \" ,  sd(New))\r\n",
        "cat(\"Variance for Variable Current: \" ,var(Current) ,\"; Variance for variable New: \",  var(New))\r\n",
        "cat(\"SD for difference between New and Current: \",sd(New-Current))"
      ],
      "execution_count": 14,
      "outputs": [
        {
          "output_type": "stream",
          "text": [
            "SD for Variable Current:  8.752985 ; SD for variable New:  9.896904Variance for Variable Current:  76.61474 ; Variance for variable New:  97.94872SD for difference between New and Current:  13.74397"
          ],
          "name": "stdout"
        }
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "LxA8-_VtzsiC"
      },
      "source": [
        "Summary of the given data shows mean and median are very close the data is normally distributed. Also summary and standard deviations for both columns says that there is no significant change in the driving distance of balls with and without coating. \r\n"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "bnqRnXglx_vM",
        "outputId": "51c4332d-6f52-419e-fb65-f79532cb3696"
      },
      "source": [
        "%%R\r\n",
        "cor(golf$Current, golf$New)"
      ],
      "execution_count": 18,
      "outputs": [
        {
          "output_type": "stream",
          "text": [
            "[1] -0.08272974\n"
          ],
          "name": "stdout"
        }
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "gd5ysJKf0EBL"
      },
      "source": [
        "Missing Value Treatment:"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "nSADDUkCvliL",
        "outputId": "56459fb5-6f94-4e25-cf49-14b5e5ce0ca4"
      },
      "source": [
        "%%R\r\n",
        "sum(is.na(golf))"
      ],
      "execution_count": 19,
      "outputs": [
        {
          "output_type": "stream",
          "text": [
            "[1] 0\n"
          ],
          "name": "stdout"
        }
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "y43Sosax0L7L"
      },
      "source": [
        "Given data doesn’t contain missing value neither in NA nor any specific condition given by the Par Inc.\r\n",
        "\r\n",
        "Outlier Treatment:"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "hTXPbVUZvrSz",
        "outputId": "0b266e3f-b4bc-4ed7-f5f1-7fd63b3893e1"
      },
      "source": [
        "%%R\r\n",
        "cat(\"Outliers in Current: \", boxplot(Current, plot=F)$Out, \" ; Outliers in New: \",boxplot(New, plot=F)$Out)"
      ],
      "execution_count": 20,
      "outputs": [
        {
          "output_type": "stream",
          "text": [
            "Outliers in Current:   ; Outliers in New: "
          ],
          "name": "stdout"
        }
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "tYJDKGHj0U1r"
      },
      "source": [
        "Result is NULL set thus there aren’t any outliers in the given data.\r\n",
        "\r\n",
        " Observations:\r\n",
        "   + 1.\tSample size:40\r\n",
        "   + 2.\tNumber of samples: 2 \r\n",
        "   + 3.\tUnpaired variables.\r\n",
        "   + 4.\tDOF = 40+40-2 = 78\r\n",
        "   + 5.\tThere are no outliers in given data, neither missing values.\r\n",
        "   + 6.\tBoth the samples seem to be normally distributed.\r\n",
        "   + 7.\tMean and median values are not much different.\r\n",
        "   + 8.\tThe Current driving distance data looks more normally distributed, whereas the driving distances data for New balls looks right skewed.\r\n",
        "   + 9.\tThere is dip in the performance of Current and New balls driving force as mean, median, min, max values differ. \r\n",
        "\r\n",
        "\r\n",
        "### Hypothesis Formulation and Testing\r\n",
        "\r\n",
        " 1.\tThe level of significance (Alpha) = 0.05\r\n",
        " 2.\tThe sample size N = 40 which is sufficiently large for a Z stat Test.\r\n",
        " 3.\tBut since the population standard deviation (Sigma) is unknown, we have to use a T stat Test.\r\n",
        " 4.\tSince the sample is different for both Sampling tests, we have N+N-2 degrees of freedom = 78\r\n",
        " 5.\tSince the sole purpose of the test is to check whether there is any effect on driving distances due to the new coating, we could prefer a Two Tailed T Test.\r\n",
        "\r\n",
        "\r\n",
        "#### Hypothesis Formulation:\r\n",
        "\r\n",
        "Use two tailed independent sample T test for means\r\n",
        "\r\n",
        " 1.\tNull Hypothesis: \r\n",
        " \r\n",
        "   + $H_{0}: µ_{old} - µ_{new} = 0$  (New coating does not have effect on driving distances)\r\n",
        " \r\n",
        " 2.\tAlternate Hypothesis: \t\r\n",
        " \r\n",
        "   + $H_{1}: µ_{old} - µ_{new} {\\ne} 0$  (New coating does have significant effect on driving distances)\r\n"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "gO98kb4ZvrVm",
        "outputId": "af1775c3-e14a-411d-d1d6-a972afac3c0c"
      },
      "source": [
        "%%R\r\n",
        "print(\"TWO TAILED INDEPENDENT TWO SAMPLE T TEST FOR MEANS\")\r\n",
        "t.test(Current, New, \r\n",
        "       paired = F, \r\n",
        "       conf.level = 0.95, \r\n",
        "       alternative = \"t\")\r\n",
        "print(\"TWO TAILED INDEPENDENT ONE SAMPLE T TEST FOR CURRENT MEAN\")\r\n",
        "t.test(Current, \r\n",
        "       paired = F, \r\n",
        "       conf.level = 0.95, \r\n",
        "       alternative = \"t\")\r\n",
        "print(\"TWO TAILED INDEPENDENT ONE SAMPLE T TEST FOR NEW MEAN\")\r\n",
        "t.test(New, \r\n",
        "       paired = F, \r\n",
        "       conf.level = 0.95, \r\n",
        "       alternative = \"t\")"
      ],
      "execution_count": 21,
      "outputs": [
        {
          "output_type": "stream",
          "text": [
            "[1] \"TWO TAILED INDEPENDENT TWO SAMPLE T TEST FOR MEANS\"\n",
            "[1] \"TWO TAILED INDEPENDENT ONE SAMPLE T TEST FOR CURRENT MEAN\"\n",
            "[1] \"TWO TAILED INDEPENDENT ONE SAMPLE T TEST FOR NEW MEAN\"\n",
            "\n",
            "\tOne Sample t-test\n",
            "\n",
            "data:  New\n",
            "t = 170.94, df = 39, p-value < 2.2e-16\n",
            "alternative hypothesis: true mean is not equal to 0\n",
            "95 percent confidence interval:\n",
            " 264.3348 270.6652\n",
            "sample estimates:\n",
            "mean of x \n",
            "    267.5 \n",
            "\n"
          ],
          "name": "stdout"
        }
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "oFNXv50f0mI6"
      },
      "source": [
        "###T-Test Conclusion\r\n",
        "\r\n",
        "####Two Tailed Two Sample Independent T Test\r\n",
        " + In this scenario, the p value is 0.188 which is greater than the 0.05.\r\n",
        " + Hence, we failed to reject the Null Hypothesis. \r\n",
        " + Thus, accepting the Null Hypothesis that there is no significant change in driving distances due to the new coating.\r\n",
        " + 95% confidence interval for difference in mean is [-1.384937  TO  6.934937]\r\n",
        "\r\n",
        "####Two Tailed One Sample T Test\r\n",
        " + 95% confidence interval for Current balls driving distance mean is [267.4757   TO   273.0743]\r\n",
        " + 95% confidence interval for New balls driving distance mean is [264.3348    TO    270.6652]\r\n",
        "\r\n",
        "\r\n",
        "### Reservations about the Result\r\n",
        " 1.\tIf we compare the means of the two sample distributions, we see that even though visually it seems as if New coating has effect on the driving distances, statistically it does not.\r\n",
        " 2.\tThe difference in mean in the case of new balls can also be attributed to the higher variance compared to Current balls.\r\n",
        " 3.\tThe variance of New balls driving distances is 97.95 is 28% more than the variance of the driving distances of Current balls 76.61.\r\n",
        " 4.\tWe are unsure of the sampling error present in the data.\r\n",
        " 5.\tStatistically there is no effect of new coating on driving distances. Though it is suggested to check the effect on the weights and other characteristics like size and shape of the new balls.\r\n",
        " 6.\tAlso, the given sample is from only one golf course, It is advisable that test should perform on different kind of golf courses to take care of the differences in grounds.\r\n",
        "\r\n",
        "### TYPE I & TYPE II Errors\r\n",
        " 1.\t**Type I Error $\\alpha$:** Probability of rejecting null hypothesis when it is true, the probability of a Type I error in hypothesis testing is predetermined by the significance level.\r\n",
        " 2.\t**Type II error $\\beta$:** Probability of failing to reject the null when it is false. Type II error calculation depends on the population mean which is unknown.\r\n",
        "\r\n"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "RuIMo3CPvrYV",
        "outputId": "2025c22f-c99f-48d0-8316-90388e89a4f0"
      },
      "source": [
        "%%R\r\n",
        "SD = sd(Current - New)\r\n",
        "cat(\"SD for difference is \", SD )\r\n",
        "\r\n",
        "DELTA = (mean(New) - mean(Current))\r\n",
        "\r\n",
        "cat(\"Difference in mean is \" , DELTA)"
      ],
      "execution_count": 22,
      "outputs": [
        {
          "output_type": "stream",
          "text": [
            "SD for difference is  13.74397Difference in mean is  -2.775"
          ],
          "name": "stdout"
        }
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "QVy8CffM1WEj"
      },
      "source": [
        "\r\n",
        "### POWER OF THE TEST AND SAMPLE SIZE\r\n",
        "\r\n",
        " 1.\tIf alternative hypothesis $µ_{New} - µ_{Current} = µ_{d} = 5$ yard as per our assumption.\r\n",
        " 2.\tNull Hypothesis $µ_{Nee} - µ_{Current} = µ_{d} = 0$\r\n",
        " 3.\tFirst we need to calculate the probability of Type I error which is predetermined by significance level. If the significance level is 0.05,\r\n",
        " 4.\tThen Type I error is 0.05 i.e. 5% probability we make Type I error – rejecting null hypothesis when it is true.\r\n",
        " 5.\tType II error calculation depends on a particular value of µ. In this case lets assume difference between population µ is 5 yard. Lets also assume that the significance level for the test is 0.05. Then the calculation is as below:\r\n",
        " 6.\tThis is a two tailed test."
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "hiQxeY0svrbK",
        "outputId": "1cb400fd-6eaf-42c9-b9f6-d524f31abd48"
      },
      "source": [
        "%%R\r\n",
        "power.t.test(n = 40, delta = DELTA, sd = SD, \r\n",
        "             type = \"t\", \r\n",
        "             alternative = \"t\", \r\n",
        "             sig.level = .05)"
      ],
      "execution_count": 23,
      "outputs": [
        {
          "output_type": "stream",
          "text": [
            "\n",
            "     Two-sample t test power calculation \n",
            "\n",
            "              n = 40\n",
            "          delta = 2.775\n",
            "             sd = 13.74397\n",
            "      sig.level = 0.05\n",
            "          power = 0.14274\n",
            "    alternative = two.sided\n",
            "\n",
            "NOTE: n is number in *each* group\n",
            "\n"
          ],
          "name": "stdout"
        }
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "VMlHKDLN1cID"
      },
      "source": [
        "Basically, the power of the test is the probability that we make the right decision when the null is not correct (i.e. we correctly reject it)\r\n",
        "\r\n",
        "### Sample Size to make Probabilities of Type I and Type II Error Equal\r\n",
        "\r\n",
        "Let us assume that, we need Type I error and Type II error equal to 0.05\r\n",
        "\r\n",
        "Assuming sample standard deviation is equal to population standard deviation, we can calculate sample size needed as below:\r\n",
        "\r\n",
        " 1.\tNull hypothesis is mean difference $µ_{0}$ is 0.\r\n",
        " 2.\tAlternative hypothesis is mean difference $µ_{1}$ is 5.\r\n",
        " 3.\tSample Standard Deviation is 13.74397.\r\n",
        " 4.\tα value is 0.05\r\n",
        " 5.\tβ value is 0.05 i.e. power of the test is 0.95 = 95 %\r\n",
        "\r\n",
        " NUMBER OF OBSERVATIONS REQUIRED FOR ALPHA = BEETA AT DELTA = 5 YARD "
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "bUEtKXuovreD",
        "outputId": "eb31ba2b-1836-4a0a-fc42-e4404b8c6588"
      },
      "source": [
        "%%R\r\n",
        "DELTA1 <- 5\r\n",
        "power.t.test(power = .95, delta = DELTA1, sd = SD, \r\n",
        "             type = \"t\", \r\n",
        "             alternative = \"t\", \r\n",
        "             sig.level = .05)"
      ],
      "execution_count": 24,
      "outputs": [
        {
          "output_type": "stream",
          "text": [
            "\n",
            "     Two-sample t test power calculation \n",
            "\n",
            "              n = 197.3383\n",
            "          delta = 5\n",
            "             sd = 13.74397\n",
            "      sig.level = 0.05\n",
            "          power = 0.95\n",
            "    alternative = two.sided\n",
            "\n",
            "NOTE: n is number in *each* group\n",
            "\n"
          ],
          "name": "stdout"
        }
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "ESMRMIN41skj"
      },
      "source": [
        "Hence, In order to retain the power, we need to round the value to next whole number. Therefore, we may conclude that we need a sample size of 198 to get the Type I and Type II Errors equal.\r\n",
        "\r\n",
        "\r\n",
        "### Conclusion:\r\n",
        "\r\n",
        "From the given data, it may be concluded that, statistically there is no significance change in driving distance due to new coating on golf balls. However, our recommendation is that the test be carried out with a larger sample size covering number of golf courses (at least a five different) to improve the accuracy of the test results and negating any effect of one type of ground. Also, the results need to interpreted and future actions be planned with the understanding of other characteristics like size, shape, weight etc.\r\n"
      ]
    }
  ]
}